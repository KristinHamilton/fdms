<?php 
//session_start();
/* Kristin Hamilton
 * created 29-Mar-2016
 * last modified 26-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

interface Pricing
{
    // Returns float total price, which includes flight base price in addition to any 
    // taxes and fees
    function getTaxesAndFees();
    function getBasePrice();
}
?>