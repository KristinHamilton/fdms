<?php
function getAirportID($connection, $airportCode){
	$query = "select airportID from Airport where airportCode = '".$airportCode."'";
	$result = $connection->query($query);
	return $result->fetch_assoc()['airportID'];
}

function getCityID($connection, $airportID){
	$query = "select cityID from Airport where airportID = ".$airportID;
	$result = $connection->query($query);
	return $result->fetch_assoc()['cityID'];
}

function getAircraftID($connection, $aircraftRegistration){
	$query = "select aircraftID from Aircraft where aircraftRegistration = '".$aircraftRegistration."'";
	$result = $connection->query($query);
	return $result->fetch_assoc()['aircraftID'];
}

function insertDeparture($connection, $depDate, $depTime, $depAirportID, $depCityID){
	$query = "insert into Departure values(Null, '".$depDate."', '".$depTime."', ".$depAirportID.", ".$depCityID.")";
	$connection->query($query);
}

function insertArrival($connection, $arrDate, $arrTime, $arrAirportID, $arrCityID){
	$query = "insert into Arrival values(Null, '".$arrDate."', '".$arrTime."', ".$arrAirportID.", ".$arrCityID.")";
	$connection->query($query);
}

function insertFlight($connection, $flightNumber, $price, $aircraftID, $departureID, $arrivalID){
	$query = "insert into Flight values(Null, '".$flightNumber."', ".$price.", ".$aircraftID.", ".$departureID.", ".$arrivalID.")";
	$connection->query($query);
}

function updateDeparture($connection, $depID, $depDate, $depTime, $depAirportID, $depCityID){
	$query = "update Departure set departureDate = '".$depDate."', departureTime = '".$depTime."', airportID = ".$depAirportID.",
			cityID = ".$depCityID." where departureID = ".$depID;
	$connection->query($query);
}

function updateArrival($connection, $arrID, $arrDate, $arrTime, $arrAirportID, $arrCityID){
	$query = "update Arrival set arrivalDate = '".$arrDate."', arrivalTime = '".$arrTime."', airportID = ".$arrAirportID.",
			cityID = ".$arrCityID." where departureID = ".$arrID;
	$connection->query($query);
}

function updateFlight($connection, $flightID, $flightNumber, $price, $aircraftID, $departureID, $arrivalID){
	$query = "update Flight set flightNumber = ".$flightNumber.", price = ".$price.", aircraftID = ".$aircraftID.",
			departureID = ".$departureID.", arrivalID = ".$arrivalID." where flightID = ".$flightID;
	$connection->query($query);
}



// Echoes an HTML document heading
function echoHead(){
	echo "<!DOCTYPE html>".
			"<html lang='en'>".
			"<head>".
			"<meta charset='utf-8'>".
			"<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'>".
			"<meta name='author' content='Nikko'>".
			"<link rel='icon' href='favicon.png' type='image/x-icon'>".
			"<link rel='stylesheet' href='flight.css' type='text/css'>".
			"<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>".
			"<link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,600,700,800' rel='stylesheet' type='text/css'>".
			"<title>Awesome Airlines</title>".
			"</head>";
}

function echoHeaderA(){
	echo "<div class='header-a'>".
			"<div class='wrapper-padding'>".
			"<div class='header-button'><a href='http://localhost/test/flightbooking/account.php'>Log Out</a></div>".
			"<div class='header-button'><a href='http://localhost/test/flightbooking/account.php'>My Account</a></div>".
			"</div>".
			"</div>";
}

function echoHeaderB($navArray){
	echo "<div class='header-b'>".
			"<div class='wrapper-padding'>".
			"<div class='header-right'>".
			"<nav class='header-nav'>".
			"<ul>";
	foreach($navArray as $link){
		echo "<li>".$link."</li>";
	}
	echo "</ul>".
			"</nav>".
			"</div>".
			"</div>".
			"</div>";
}

function echoFooterB(){
	echo "<div class='footer-b'>".
			"<div class='wrapper-padding'>".
			"<div class='footer-left'>Copyright &copy 2016 Awesome Airlines. All rights reserved.</div>".
			"</div>".
			"</div>";
}

// Database User functions

function getUserID($connection, $username){
	$query = "select userID from User where username = '".$username."'";
	$result = $connection->query($query);
	return $result->fetch_assoc()['userID'];
}

function getUser($connection, $userID){
	$query = "select username, firstName, lastName, DoB, email, phone from User where userID = ".$userID;
	$result = $connection->query($query);
	$row = $result->fetch_assoc();
	return $row;
}

function setFirstName($connection, $userID, $firstName){
	$query = "update User set firstName = '".$firstName."' where userID = ".$userID;
	$connection->query($query);
}

function setLastName($connection, $userID, $lastName){
	$query = "update User set lastName = '".$lastName."' where userID = ".$userID;
	$connection->query($query);
}

function setEmail($connection, $userID, $email){
	$query = "update User set email = '".$email."' where userID = ".$userID;
	$connection->query($query);
}

function setPhone($connection, $userID, $phone){
	$query = "update User set phone = ".$phone." where userID = ".$userID;
	$connection->query($query);
}

// Database Aircraft functions

function getAircraft($connection){
	$query = "select aircraftID, aircraftRegistration, modelName from Aircraft, Model where Aircraft.modelID = Model.modelID";
	$result = $connection->query($query);
	return $result;
}

function getAircraftModels($connection){
	$query = "select modelID, modelName from Model";
	$result = $connection->query($query);
	return $result;
}

function insertAircraft($connection, $aircraftRegistration, $modelID){
	$query = "insert into Aircraft values(Null, '".$aircraftRegistration."', ".$modelID.")";
	$connection->query($query);
}

function deleteAircraft($connection, $aircraftID){
	$query = "delete from Aircraft where aircraftID = ".$aircraftID;
	$connection->query($query);
}

// Database Flight functions

function getFlights($connection){
	$query = "select flightID, flightNumber, aircraftRegistration, price, departureDate, departureTime, arrivalDate, arrivalTime 
			from Flight, Aircraft, Departure, Arrival where Flight.aircraftID = Aircraft.aircraftID 
			and Flight.departureID = Departure.departureID and Flight.arrivalID = Arrival.arrivalID";
	$result = $connection->query($query);
	return $result;
}

function getFlight($connection, $flightID){
	$query = "select flightNumber, aircraftRegistration, price, Flight.departureID, departureDate, departureTime, flight.arrivalID, arrivalDate, arrivalTime 
			from Flight, Aircraft, Departure, Arrival where Flight.aircraftID = Aircraft.aircraftID 
			and Flight.departureID = Departure.departureID and Flight.arrivalID = Arrival.arrivalID and Flight.flightID = ".$flightID;
	$result = $connection->query($query);
	$row = $result->fetch_assoc();
	return $row;
}

function getUserFlights($connection, $userID){
	$query = "select flightID, flightNumber, departureDate, departureTime, arrivalDate, arrivalTime from Flight, Departure, Arrival 
			where Flight.departureID = Departure.departureID and Flight.arrivalID = Arrival.arrivalID 
			and flightID in(select flightID from FlightUser where userID = ".$userID.")";
	$result = $connection->query($query);
	return $result;
}

function getDepartureAirportCode($connection, $flightID){
	$query = "select airportCode from Airport, Departure, Flight where Airport.airportID = Departure.airportID
			and Departure.departureID = Flight.departureID and flightID = ".$flightID;
	$result = $connection->query($query);
	return $result->fetch_assoc()['airportCode'];
}

function getDepartureAirportName($connection, $flightID){
	$query = "select airportName from Airport, Departure, Flight where Airport.airportID = Departure.airportID 
			and Departure.departureID = Flight.departureID and flightID = ".$flightID;
	$result = $connection->query($query);
	return $result->fetch_assoc()['airportName'];
}

function getArrivalAirportCode($connection, $flightID){
	$query = "select airportCode from Airport, Arrival, Flight where Airport.airportID = Arrival.airportID
			and Arrival.arrivalID = Flight.arrivalID and flightID = ".$flightID;
	$result = $connection->query($query);
	return $result->fetch_assoc()['airportCode'];
}

function getArrivalAirportName($connection, $flightID){
	$query = "select airportName from Airport, Arrival, Flight where Airport.airportID = Arrival.airportID 
			and Arrival.arrivalID = Flight.arrivalID and flightID = ".$flightID;
	$result = $connection->query($query);
	return $result->fetch_assoc()['airportName'];
}

function deleteFlight($connection, $flightID){
	$query = "delete from Flight where flightID = ".$flightID;
	$connection->query($query);
}

// Database Payment functions

function getPaymentTypes($connection){
	$query = "select paymentTypeID, paymentTypeName from PaymentType";
	$result = $connection->query($query);
	return $result;
}

function getUserPayments($connection, $userID){
	$query = "select paymentDate, paymentAmount from Payment where userID = ".$userID;
	$result = $connection->query($query);
	return $result;
}

function getUserPaymentMethods($connection, $userID){
	$query = "select paymentMethodID, paymentTypeName, cardNumber, cardHolderName, expirationYear, expirationMonth, securityCode 
			from PaymentType, PaymentMethod where PaymentType.paymentTypeID = PaymentMethod.paymentTypeID and userID = ".$userID;
	$result = $connection->query($query);
	return $result;
}

function insertPaymentMethod($connection, $paymentTypeID, $cardNumber, $cardHolderName, $expirationYear, $expirationMonth, $securityCode, $userID){
	$query = "insert into PaymentMethod values(Null, ".$paymentTypeID.", ".$cardNumber.", '".$cardHolderName."', 
			".$expirationYear.", ".$expirationMonth.", ".$securityCode.", ".$userID.")";
	$connection->query($query);
}

function deletePaymentMethod($connection, $paymentMethodID){
	$query = "delete from PaymentMethod where paymentMethodID = ".$paymentMethodID;
	$connection->query($query);
}
?>