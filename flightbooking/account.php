<?php
require "connect.php";
require "flightFunctions.php";

echoHead();

session_start();
$connection = connect();
$_SESSION["userID"] = getUserID($connection, $_SESSION["username"]);
$connection->close();

echo "<body>";
echoHeaderA();
$navArray = array(
		"<a href='paymentMethods.php'>Payment Methods</a>",
		"<a href='paymentHistory.php'>Payment History</a>",
		"<a href='flightHistory.php'>Flight History</a>"
);
echoHeaderB($navArray);
echo "<div class='content'>";

updateSession();
if(isset($_POST['edit'])){
	editAccountInfo($_SESSION["firstName"], $_SESSION["lastName"], $_SESSION["email"], $_SESSION["phone"]);
}
elseif(isset($_POST['save'])){
	checkAccountInfo();
}
else{
	viewAccountInfo();
}

echo "</div>";
echoFooterB();
echo "</body>";

// Displays the account information page
function viewAccountInfo(){
	echo "<div class='heading'>Account Information</div>".
			"<form method='post'>".
			"<input type='submit' class='button' name='edit' value='Edit Account'>".
			"</form>".
			"<div class='subheading'>Personal Information</div>".
			"<div class='row'>First Name: ".$_SESSION["firstName"]."</div>".
			"<div class='row'>Last Name: ".$_SESSION["lastName"]."</div>".
			"<div class='row'>Date of Birth: ".date("m/d/Y", strtotime($_SESSION['DoB']))."</div>".
			"<div class='subheading'>Contact Information</div>".
			"<div class='row'>Email: ".$_SESSION["email"]."</div>".
			"<div class='row'>Phone: ";
	if(!isset($_SESSION["phone"]) || $_SESSION["phone"] == null){
		echo "</div>";
	}
	else{
		preg_match_all("/(\d{3})(\d{3})(\d{4})/", $_SESSION["phone"], $matches);
		echo $matches[1][0]."-".$matches[2][0]."-".$matches[3][0]."</div>";
	}
}

// Displays the edit account information form
function editAccountInfo($firstName, $lastName, $email, $phone){
	echo "<div class='heading'>Edit Account Information</div>".
			"<form method='post'>".
			"<div class='subheading'>Personal Information</div>".
			"<div class='form-row'>".
			"<div class='title'>First Name:</div>".
			"<div class='field'><input type='text'name='firstName' value='".$firstName."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Last Name:</div>".
			"<div class='field'><input type='text' name='lastName' value='".$lastName."'></div>".
			"</div>".
			"<div class='subheading'>Contact Information</div>".
			"<div class='form-row'>".
			"<div class='title'>Email:</div>".
			"<div class='field'><input type='text'name='email' value='".$email."'></div>".
			"</div>".
			"<div class='form-row'>".
			"<div class='title'>Phone:</div>".
			"<div class='field'><input type='text' maxlength='10' name='phone' value='".$phone."'></div>".
			"</div>".
			"<div class='subheading'></div>".
			"<input type='submit' class='button' name='save' value='Save'>".
			"<div class='space'></div>".
			"<input type='submit' class='button' name='cancel' value='Cancel'>".
			"</form>";
}

// Validates edit account information form data
function checkAccountInfo(){
	$firstName = trim(htmlentities($_POST['firstName'], ENT_QUOTES));
	$lastName = trim(htmlentities($_POST['lastName'], ENT_QUOTES));
	$email = trim(htmlentities($_POST['email'], ENT_QUOTES));
	$phone = trim(htmlentities($_POST['phone'], ENT_QUOTES));
	$error = "";
	if(!preg_match_all("/[A-Za-z]+/", $firstName, $matches)){
		$error = "Please enter a valid first name.";
	}
	elseif(!preg_match_all("/[A-Za-z]+/", $lastName, $matches)){
		$error = "Please enter a valid last name.";
	}
	elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$error = "Please enter a valid email address.";
	}
	elseif($phone != null && !preg_match_all("/\d{10}/", $phone, $matches)){
		$error = "Please enter a valid phone number.";
	}
	if($error){
		echo "<font color='red'>".$error."</font>";
		editAccountInfo($firstName, $lastName, $email, $phone);
	}
	else{
		updateUser($firstName, $lastName, $email, $phone);
		updateSession();
		viewAccountInfo();
	}
}

// Updates user information in the flight database
function updateUser($firstName, $lastName, $email, $phone){
	$connection = connect();
	$userID = $_SESSION["userID"];
	if($_SESSION["firstName"] != $firstName){
		setFirstName($connection, $userID, $firstName);
	}
	if($_SESSION["lastName"] != $lastName){
		setLastName($connection, $userID, $lastName);
	}
	if($_SESSION["email"] != $email){
		setEmail($connection, $userID, $email);
	}
	if($_SESSION["phone"] != $phone){
		if($phone == null){
			$phone = "Null";
		}
		setPhone($connection, $userID, $phone);
	}
	$connection->close();
}

// Updates user session variables
function updateSession(){
	$connection = connect();
	$userID = $_SESSION["userID"];
	$user = getUser($connection, $userID);
	$connection->close();
	$_SESSION["username"] = $user['username'];
	$_SESSION["firstName"] = $user['firstName'];
	$_SESSION["lastName"] = $user['lastName'];
	$_SESSION["DoB"] = $user['DoB'];
	$_SESSION["email"] = $user['email'];
	$_SESSION["phone"] = $user['phone'];
}
?>