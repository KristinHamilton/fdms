<?php
echo "<!DOCTYPE html>".
		"<html lang='en'>".
		"<head>".
		"<meta charset='utf-8'>".
		"<meta name='viewport' content='width=device-width, initial scale=1.0'>".
		"<meta name='author' content='Nikko'>".
		"<link rel='icon' href='./favicon.ico' type='image/x-icon'>".
		"<link rel='shortcut icon' href='./favicon.ico' type='image/x-icon'>".
		"<link rel='stylesheet' href='./flight.css' type='text/css'>".
		"<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Raleway' type='text/css'>".
		"<title>Flight</title>".
		"</head>";

require "connect.php";
require "flightDatabaseFunctions.php";

session_start();
$connection = connect();
$_SESSION["userID"] = getUserID($connection, $_SESSION["username"]);
$connection->close();

echo "<body>";
echo "<header class='header'></header>";
echo "<div class='content'>";

viewFlightHistory();

echo "</div>";
echo "<div class='footerContainer'>";
echo "<div class='footer'>Copyright &copy 2016 Awesome Airlines. All rights reserved.</div>";
echo "</div>";
echo "</body>";

function viewFlightHistory(){
	echo "<h4>Flight History</h4>".
			"<table class='table'>".
				"<tr>".
					"<th>Departure Date</th>".
					"<th>Departure Time</th>".
					"<th>Departure Airport</th>".
					"<th>Arrival Date</th>".
					"<th>Arrival Time</th>".
					"<th>Arrival Airport</th>".
				"</tr>";
	$connection = connect();
	$result = getUserFlights($connection, $_SESSION["userID"]);
	while($row = $result->fetch_assoc()){
		$flightID = $row['flightID'];
		$departure = getDeparture($connection, $flightID);
		$arrival = getArrival($connection, $flightID);
		echo "<tr>".
					"<td>".formatDate($departure['departureDate'])."</td>".
					"<td>".formatTime($departure['departureTime'])."</td>".
					"<td>".getDepartureAirport($connection, $flightID)."</td>".
					"<td>".formatDate($arrival['arrivalDate'])."</td>".
					"<td>".formattime($arrival['arrivalTime'])."</td>".
					"<td>".getArrivalAirport($connection, $flightID)."</td>".
				"</tr>";
	}
	$connection->close();
	echo "</table>";
}
?>