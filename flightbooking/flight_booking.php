<?php
require_once "../booking/Customer.php";
require_once "../booking/CustomerImpl.php";
require_once "../booking/Flight.php";
require_once "../booking/FlightImpl.php";
require_once "../booking/Booking.php";
require_once "../booking/BookingImpl.php";
require_once "../pricing/Pricing.php";
require_once "../pricing/PricingImpl.php";
require_once "../booking/BookingPayment.php";
require_once "../booking/BookingPaymentImpl.php";

session_start();
/* Kristin Hamilton
 * created 29-Mar-2016
 * Desc: Navigation logic for proceeding through booking process
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

require_once "../util/utilFunctions.php";
require_once "../booking/booking_getUserInfo.php";
require_once "../booking/booking_getFlightInfo.php";
require_once "../booking/booking_getValuesFromPostArray.php";
require_once "../booking/booking_presentationLayer.php";
require_once "../booking/booking_formValidation.php";

//echo "<br />\$_POST = "; var_dump($_POST); echo "<br />";
//echo "<br />\$_SESSION = <br />"; var_dump($_SESSION); echo "<br />";

$flightID1 = -1;
$flightID2 = -1;
$travelerCount = -1;
$customerInfo = getGuestCustomerInfo();

if(isset($_SESSION['status']) && $_SESSION['status'] == 1)
{
    $customerInfo = getCustomerInfoFromSessionArray();
}
$customer = new CustomerImpl($customerInfo);
    

if(isset($_POST['selectRet']) || empty($_POST))  // || 
{
    // departure, arrival, deptDate, retDate, people, deptFlightID, retFlightID
    if(isset($_POST['deptFlightID']))
    {
        $flightID1 = htmlentities($_POST['deptFlightID']);
    }
    else  // TODO: remove!
    {
        die("<br />Error: deptFlightID not found<br />");  // die("")
        //$flightID1 = 1;
    }
    
    if(isset($_POST['retFlightID']))
    {
        $flightID2 = htmlentities($_POST['retFlightID']);
    }
    else  // TODO: remove!
    {
        die("<br />Error: retFlightID not found<br />");  // die("")
        //$flightID2 = 3;
    }

    if(isset($_POST['people']))
    {
        $travelerCount = htmlentities($_POST['people']);
    }
    else
    {
        die("<br />Error: people not found<br />");  // die("")
         //$travelerCount = 3;  // TODO: remove!
    }
    
    $flight1 = new FlightImpl($flightID1, $travelerCount);
    $flight2 = new FlightImpl($flightID2, $travelerCount);

    $_SESSION['booking'] = new BookingImpl($flight1, $flight2);
    $booking = $_SESSION['booking'];
    $booking->setCustomer($customer);
    displayForm_travelerInfo($booking);  // displayForm_seatSelection($booking); 
}
else if(isset($_POST['submitSeatSelection']))
{     
    $booking = $_SESSION['booking'];
    // TODO: if seat selection left blank, need to assign the seats before setting
    // seat selection for this booking. also need to check for any 
    $seatSelection = getSeatSelectionFromPostArray();
    $booking->setSeatSelection($seatSelection);
    displayForm_travelerInfo($booking);
}
else if(isset($_POST['submitTravelerInfo']))
{
    $booking = $_SESSION['booking'];
    $passengerList = getPassengerInfoFromPostArray();
    $booking->setPassengerList($passengerList);
    
    if(isset($_POST['customerEmailAddress']))
    {
        //if($customer->getCustomerEmail() == '')
        //{
            $customerEmail = htmlentities($_POST['customerEmailAddress']);
            if($customerEmail != '')
            {                    
                $customer->setCustomerEmail($customerEmail);
            }
        //}
    }
    
    if(isset($_POST['customerPhoneNumber']))
    {
        //if($customer->getCustomerPhone(false) == '')
        //{
            $customerPhone = htmlentities($_POST['customerPhoneNumber']);
            if($customerPhone != '')
            {
                $customer->setCustomerPhone($customerPhone);
            }
        //}
    }
    
    displayForm_paymentInfo(false, $booking);
}
else if(isset($_POST['submitPaymentInfo']))
{    
    $booking = $_SESSION['booking'];
    $paymentInfo = getPaymentInfoFromPostArray();
    $bookingPayment = new BookingPaymentImpl($paymentInfo);
    $booking->setBookingPayment($bookingPayment);
    displayForm_confirmPayment($booking);       
}
else if(isset($_POST['submitConfirmPayment']))
{ 
    $paymentSuccessful = false;
    $booking = $_SESSION['booking'];
    // Try to process payment. if successful, proceed to booking confirmation display page
    $bookingPayment = $booking->getBookingPayment();
    $paymentSuccessful = $booking->processPayment($bookingPayment);
    
    if($paymentSuccessful)
    {
        if(isset($_SESSION['status']) && $_SESSION['status'] == 1 && 
           $paymentSuccessful) 
        {
            require_once "../booking/updateDbs.php";
            updateDbs($booking);
        }
        
        require_once "../booking/booking_complete.php";
        displayForm_bookingConfirmation($booking);
    }   
    else
    {
        displayForm_paymentInfo(true, $booking);
    }        
}
else if(isset($_POST['submitPrint']))
{
    // print the confirmation page
    echo "<i>Printing booking confirmation...</i>";  // TODO
}
else if(isset($_POST['logout']))
{
    session_destroy();
    header("location: index.php");
}
else if(isset($_POST['login']))
{
    header("location: index.php");
}
else
{
    echo "<br />Error: undefined behavior<br />\$_POST = ";
    var_dump($_POST);
    echo "<br />";
}
?>