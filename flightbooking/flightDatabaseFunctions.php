<?php

// User functions

function getUserID($connection, $username){
	$query = "select userID from User where username = '".$username."'";
	$result = $connection->query($query);
	return $result->fetch_assoc()['userID'];
}

function getUser($connection, $userID){
	$query = "select username, firstName, lastName, DoB, email, phone from User where userID = ".$userID;
	$result = $connection->query($query);
	$row = $result->fetch_assoc();
	return $row;
}

function setFirstName($connection, $userID, $firstName){
	$query = "update User set firstName = '".$firstName."' where userID = ".$userID;
	$connection->query($query);
}

function setLastName($connection, $userID, $lastName){
	$query = "update User set lastName = '".$lastName."' where userID = ".$userID;
	$connection->query($query);
}

function setEmail($connection, $userID, $email){
	$query = "update User set email = '".$email."' where userID = ".$userID;
	$connection->query($query);
}

function setPhone($connection, $userID, $phone){
	$query = "update User set phone = ".$phone." where userID = ".$userID;
	$connection->query($query);
}

function setStatus($connection, $userID, $status){
	if($status > 0 && $status < 3){
		$query = "update User set status = ".$status." where userID = ".$userID;
		$connection->query($query);
	}
}

// Flight functions

function getUserFlights($connection, $userID){
	$query = "select flightID from FlightUser where userID = ".$userID;
	$result = $connection->query($query);
	return $result;
}

function getDeparture($connection, $flightID){
	$query = "select departureDate, departureTime from Departure, Flight where Departure.departureID = Flight.departureID 
			and flightID = ".$flightID;
	$result = $connection->query($query);
	$row = $result->fetch_assoc();
	return $row;
}

function getDepartureAirport($connection, $flightID){
	$query = "select airportName from Airport, Departure, Flight where Airport.airportID = Departure.airportID 
			and Departure.departureID = Flight.departureID and flightID = ".$flightID;
	$result = $connection->query($query);
	return $result->fetch_assoc()['airportName'];
}

function getArrival($connection, $flightID){
	$query = "select arrivalDate, arrivalTime from Arrival, Flight where Arrival.arrivalID = Flight.arrivalID and flightID = ".$flightID;
	$result = $connection->query($query);
	$row = $result->fetch_assoc();
	return $row;
}

function getArrivalAirport($connection, $flightID){
	$query = "select airportName from Airport, Arrival, Flight where Airport.airportID = Arrival.airportID 
			and Arrival.arrivalID = Flight.arrivalID and flightID = ".$flightID;
	$result = $connection->query($query);
	return $result->fetch_assoc()['airportName'];
}

// Payment functions

function getUserPayments($connection, $userID){
	$query = "select paymentID from Payment where userID = ".$userID;
	$result = $connection->query($query);
	return $result;
}

function getPayment($connection, $paymentID){
	$query = "select paymentDate, paymentAmount from Payment where paymentID = ".$paymentID;
	$result = $connection->query($query);
	$row = $result->fetch_assoc();
	return $row;
}

function getPaymentType($connection){
	$query = "select paymentTypeID, paymentTypeName from PaymentType";
	$result = $connection->query($query);
	return $result;
}

function getUserPaymentMethods($connection, $userID){
	$query = "select paymentMethodID from PaymentMethod where userID = ".$userID;
	$result = $connection->query($query);
	return $result;
}

function getPaymentMethod($connection, $paymentMethodID){
	$query = "select paymentTypeName, cardNumber from PaymentType, PaymentMethod where PaymentType.paymentTypeID = PaymentMethod.paymentTypeID 
			and paymentMethodID = ".$paymentMethodID;
	$result = $connection->query($query);
	$row = $result->fetch_assoc();
	return $row;
}

function insertPaymentMethod($connection, $paymentTypeID, $cardNumber, $userID){
	$query = "insert into PaymentMethod values(Null, ".$paymentTypeID.", ".$cardNumber.", ".$userID.")";
	$connection->query($query);
}

function deletePaymentMethod($connection, $paymentMethodID){
	$query = "delete from PaymentMethod where paymentMethodID = ".$paymentMethodID;
	$connection->query($query);
}

// Format functions

function formatDate($date){
	preg_match_all("/(\d{4})-(\d{2})-(\d{2})/", $date, $matches);
	$dateFormat = $matches[2][0]."/".$matches[3][0]."/".$matches[1][0];
	return $dateFormat;
}

function formatTime($time){
	preg_match_all("/(\d{2}):(\d{2}):\d{2}/", $time, $matches);
	$hour = $matches[1][0] % 12;
	if($hour == 0){
		$timeFormat = $hour.":".$matches[2][0]." a.m.";
	}
	else{
		$timeFormat = $hour.":".$matches[2][0]." p.m.";
	}
	return $timeFormat;
}
?>