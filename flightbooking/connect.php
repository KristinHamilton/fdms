<?php
function connect(){
	$host = "localhost";
	$username = "root";
	$password = "root";
	$database = "FlightDB";
	$port = "3306";
	$connection = new mysqli($host, $username, $password, $database, $port);
	if($connection->connect_error){
		die($connection->connect_error);
	}
	return $connection;
}
?>