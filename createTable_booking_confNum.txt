--create table script for booking table and confNum table
=======
--Kristin Hamilton
--created 04-Apr-2016
--last modified 04-Apr-2016
--Desc: create table script for booking table and confNum table

/* create booking table */

CREATE TABLE IF NOT EXISTS booking (
    bookingId INT UNSIGNED AUTO_INCREMENT,
    userId INT NOT NULL,
    confNum CHAR(16) NOT NULL UNIQUE,
    flightId INT UNSIGNED NOT NULL,
    ticketCount INT UNSIGNED NOT NULL,
    PRIMARY KEY(bookingId),
    FOREIGN KEY(confNum) REFERENCES confNum(confNum)
) engine=innodb;

/* create confNum table to hold confirmation numbers */

CREATE TABLE IF NOT EXISTS confNum (
    confNumId INT UNSIGNED AUTO_INCREMENT,
    confNum CHAR(16) NOT NULL UNIQUE,
    PRIMARY KEY(confNumId)
) engine=innodb;
