<?php
require "connect.php";
require "flightFunctions.php";

echoHead();

session_start();

echo "<body>";
echoHeaderA();
$navArray = array(
		"<a href='http://localhost/test/flightbooking/paymentMethods.php'>Payment Methods</a>",
		"<a href='http://localhost/test/flightbooking/paymentHistory.php'>Payment History</a>",
		"<a href='http://localhost/test/flightbooking/flightHistory.php'>Flight History</a>"
);
echoHeaderB($navArray);
echo "<div class='content'>";

viewPaymentHistory();

echo "</div>";
echoFooterB();
echo "</body>";

// Displays the payment history page
function viewPaymentHistory(){
	echo "<div class='heading'>Payment History</div>";
	$connection = connect();
	$result = getUserPayments($connection, $_SESSION["userID"]);
	if($result->num_rows > 0){
		echo "<table class='table'>".
				"<tr>".
				"<th>Payment Date</th>".
				"<th>Amount</th>".
				"<tr>";
		while($row = $result->fetch_assoc()){
			echo "<tr>".
					"<td>".date("m/d/Y", strtotime($row['paymentDate']))."</td>".
					"<td>$".$row['paymentAmount']."</td>".
					"</tr>";
		}
		echo "</table>";
	}
	else{
		echo "No payments are associated with this account.";
	}
	$connection->close();
}
?>