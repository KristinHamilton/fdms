<?php
/* Kristin Hamilton */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

function dbConnect()
{
    $host = "localhost";
    $user = "root";
    $pw = "root";
    $database = "FlightDB";
    $db = mysqli_connect($host, $user, $pw, $database);
    
    if(!$db)
    {
        // if can't connect to localhost as backup, die + show error info
        die('oops connection problem ! --> '.mysql_error());
    }
    else
    {
        return $db;  // return connection to localhost database
    }
}
?>