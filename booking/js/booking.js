/**
 * booking.js
 */

function setPaymentTypeName(paymentType)
{
    var elemtId = 'submitPaymentInfo';
    //alert("paymentType = " + paymentType + "\ndivId = " + elemtId);
    document.getElementById(elemtId).value = paymentType;
}

function validate()
{
    var validated = true;
    
    //var form = document.forms[formName];
    
    return validated;
}

function getDropDownOptionsHtml_ints(start, end, defaultText, selectedOption)
{
    var html = "";
    
    html += "<option";
    if(selectedOption == -1){ html += " selected"; }
    html += ">" + defaultText + "</option>";
    
    for(var i = start; i < end + 1; i++)
    {
        if(selectedOption > -1 && i == selectedOption)
        {
            html += "<option selected>" + i + "</option>";
        }
        else
        {
            html += "<option>" + i + "</option>";
        }
    }

    return html;
}

function addPassengerInfoForm()
{
    var divId = "additionalPassengers";
    
    var html = "<div class='booking-form'><div class='booking-form-i'>" + 
    "<label>First Name:</label>" + 
    "<div class='input'><input type='text' name='passengerFirstName[]' value='' /></div>" +
    "</div><div class='booking-form-i'><label>Last Name:</label>" + 
    "<div class='input'><input type='text' name='passengerLastName[]' value='' /></div>" + 
    "</div><div class='booking-form-i'><div class='form-calendar'>" +
    "<label>Date of Birth:</label><div class='form-calendar-a'>" +
    "<select class='custom-select' name='passengerDobDay[]'>";

    html += getDropDownOptionsHtml_ints(1, 31, 'Day', -1);
    
    html += "</select></div><div class='form-calendar-a'>" +
            "<select class='custom-select' name='passengerDobMonth[]'>";
    
    html += getDropDownOptionsHtml_ints(1, 12, 'Month', -1);
    
    html += "</select></div><div class='form-calendar-b'>" + 
            "<select class='custom-select' name='passengerDobYear[]'>";
    
    var currentYear = new Date().getFullYear();
    html += getDropDownOptionsHtml_ints(currentYear - 150, currentYear, 'Year', -1);
    
    html += "</select></div></div><div class='clear'></div></div><div class='clear'></div>" +
            "<div class='booking-devider'></div>";
    
    document.getElementById(divId).innerHtml = html;
    //document.write(html);
}