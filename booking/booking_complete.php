<?php
//session_start();
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

function displayForm_bookingConfirmation($booking)
{
printPageHeader("Booking Confirmation", "Booking Confirmation");

/*echo "<div>booking_complete.php: \$_POST = <br />";
var_dump($_POST);
echo "<br />booking_complete.php: \$_SESSION = <br />";
var_dump($_SESSION);
echo "</div>";*/

$flight1 = $booking->getFlight1();
$flight2 = $booking->getFlight2();
$passengerList = $booking->getPassengerList();
$customer = $booking->getCustomer();
$bookingPayment = $booking->getBookingPayment();
$bookingPaymentTypeName = $bookingPayment->getBookingPaymentTypeName();
$paymentTypeName = strtoupper($bookingPaymentTypeName[0]) . substr($bookingPaymentTypeName, 1); 
$confNumber = $booking->getConfirmationNumber();

echo
"    <div class='sp-page'>
        <div class='sp-page-a'>
            <div class='sp-page-l'>
                  <div class='sp-page-lb'>
                      <div class='sp-page-p'>
                        <div class='booking-left'>
                            <h2>Booking Complete</h2>
                                Your confirmation number is: $confNumber
                            <div class='comlete-alert'>
                                <div class='comlete-alert-a'>
                                    <b>Thank You. Your Order is Confirmed.</b>
                                    <span>Please print this page for your own records.</span><br />
                                </div>
                            </div>";

    if(isset($_SESSION['status']) && $_SESSION['status'] == 1)
    {
        echo "                                                        
                            <div class='complete-info'>
                                <h2>Your Personal Information</h2>
                                <div class='complete-info-table'>
                                    <div class='complete-info-i'>
                                        <div class='complete-info-l'>First Name:</div>
                                        <div class='complete-info-r'>".
                                        $customer->getCustomerFirstName() ."</div>
                                        <div class='clear'></div>
                                    </div>
                                    <div class='complete-info-i'>
                                        <div class='complete-info-l'>Last Name:</div>
                                        <div class='complete-info-r'>". 
                                        $customer->getCustomerLastName() ."</div>
                                        <div class='clear'></div>
                                    </div>
                                    <div class='complete-info-i'>
                                        <div class='complete-info-l'>E-Mail Adress:</div>
                                        <div class='complete-info-r'>". 
                                        $customer->getCustomerEmail() ."</div>
                                        <div class='clear'></div>
                                    </div>
                                    <div class='complete-info-i'>
                                        <div class='complete-info-l'>Phone Number:</div>
                                        <div class='complete-info-r'>". 
                                        $customer->getCustomerPhone(true) ."</div>
                                        <div class='clear'></div>
                                    </div>
                                </div>
                                
                                <div class='complete-devider'></div>";
    }   
    echo "
                                
                                <div class='complete-txt'>
                                    <h2>Payment Summary</h2>
                                    <div class='complete-info-table'>
                                        <div class='complete-info-i'>
                                            <div class='complete-info-l'>Total Price:</div>
                                            <div class='complete-info-r'>\$". 
                                            round($booking->getFinalPriceTotal(), 2) ."</div>
                                            <div class='clear'></div>
                                        </div>
                                        <div class='complete-info-i'>
                                            <div class='complete-info-l'>Payment Method:</div>
                                            <div class='complete-info-r'>". 
                                            $paymentTypeName ."</div>
                                            <div class='clear'></div>
                                        </div>
                                        <div class='complete-info-i'>
                                            <div class='complete-info-l'>Card Number:</div>
                                            <div class='complete-info-r'>**** **** **** ".
                                            substr($bookingPayment->getBookingPaymentCardNumber(), 12, 15) ."</div>
                                            <div class='clear'></div>
                                        </div>
                                        <div class='complete-info-i'>
                                            <div class='complete-info-l'>Cardholder Name</div>
                                            <div class='complete-info-r'>". 
                                            $bookingPayment->getBookingPaymentCardholderName() ."</div>
                                            <div class='clear'></div>
                                        </div>
                                        <div class='complete-info-i'>
                                            <div class='complete-info-l'>Card Expiration:</div>
                                            <div class='complete-info-r'>". 
                                            $bookingPayment->getBookingPaymentCardExpirationMonth() ."/". 
                                            $bookingPayment->getBookingPaymentCardExpirationYear() ."</div>
                                            <div class='clear'></div>
                                        </div>                                        
                                    </div>
                                </div>
                                                    
                                 <div class='complete-devider'></div>
                                
                                <div class='complete-txt'>
                                    <h2>Passenger List</h2>
                                        <div class='complete-info-table'>
                                            <div class='complete-info-i'>
                                                <div class='complete-info-l'><b>Passenger Name</b></div>
                                                <div class='complete-info-r'><b>Date of Birth</b></div>
                                                <div class='clear'></div>
                                            </div>";
                                            
                                    for($i = 0; $i < count($passengerList); $i++)
                                    {
                                        $passengerName = $passengerList[$i]['firstName'] ." ".
                                                $passengerList[$i]['lastName'];
                                        $passengerDoB = $passengerList[$i]['DoB']['month'] ."/".
                                                $passengerList[$i]['DoB']['month'] ."/".
                                                $passengerList[$i]['DoB']['year'];
                                        
                                        echo "
                                            <div class='complete-info-i'>
                                                <div class='complete-info-l'>$passengerName</div>
                                                <div class='complete-info-r'>$passengerDoB</div>
                                                <div class='clear'></div>
                                            </div>";
                                    }

                                    echo "
                                    </div>
                                </div>                                                   
                                                    
                                <div class='complete-devider'></div>
                                
                                <div class='complete-txt final'>
                                    <h2>Flight Itinerary</h2>";

/*  <day of week>, <Month> <Day>
	<departureAirportCity_1> (<departureAirportCode_1>) to <arrivalAirportCity_1> (<arrivalAirportCode_1>)
	Depart <departureTime_1>  Arrive <arrivalTime_1> (_h _m)
	Flight # <FlightNumber_1> | <aircraftRegistration_1> | <model_1> */

                                        echo "
                                    <p>
                                        <span style='color:#FF7200;'><b>LEAVING</b></span><br />";

$deptDate = $flight1->getFlightDepartureDate(true);

$deptCity = $flight1->getFlightDepartureAirportCity();
$deptState = $flight1->getFlightDepartureAirportState();
$deptAirportCode = $flight1->getFlightDepartureAirportCode();
$deptTime = $flight1->getFlightDepartureTime();

$arvCity = $flight1->getFlightArrivalAirportCity();
$arvState = $flight1->getFlightArrivalAirportState();
$arvAirportCode = $flight1->getFlightArrivalAirportCode();    
$arvTime = $flight1->getFlightArrivalTime();

$flightNumber = $flight1->getFlightNumber();
$aircraftReg = $flight1->getFlightAircraftRegistration();
$model = $flight1->getFlightModelName();

$flightDuration = $arvTime - $deptTime;

echo "
        <span style='color:#4A90A4;'><b>$deptDate</b></span><br />
        <div class='complete-info-table'>
            <div class='complete-info-i'>
                $deptCity,&nbsp;$deptState&nbsp;($deptAirportCode)&nbsp;&nbsp;to&nbsp;&nbsp;".
                "$arvCity,&nbsp;$arvState&nbsp;($arvAirportCode)
            </div>
            <div class='complete-info-i'>
                DEPART&nbsp;$deptTime&nbsp;&nbsp;ARRIVE&nbsp;$arvTime&nbsp;&nbsp;(&nbsp;$flightDuration h&nbsp;)
            </div>";
echo "
            <div class='complete-info-i'>
                FLIGHT&nbsp;$flightNumber&nbsp;|&nbsp;$aircraftReg&nbsp;|&nbsp;$model<br />
            </div>";
echo " 
        </div>       
                                    </p>";

/* 	<day of week>, <Month> <Day>
	<departureAirportCity_2> (<departureAirportCode_2>) to <arrivalAirportCity_2> (<arrivalAirportCode_2>)
	Depart <departureTime_2>  Arrive <arrivalTime_2> (_h _m)
	Flight # <FlightNumber_2> | <aircraftRegistration_2> | <model_2> */                                        
                    
echo
"                                   <p>
                                        <span style='color:#FF7200;'><b>RETURNING</b></span><br />";
        
$deptDate = $flight2->getFlightDepartureDate(true);

$deptCity = $flight2->getFlightDepartureAirportCity();
$deptState = $flight2->getFlightDepartureAirportState();
$deptAirportCode = $flight2->getFlightDepartureAirportCode();
$deptTime = $flight2->getFlightDepartureTime();

$arvCity = $flight2->getFlightArrivalAirportCity();
$arvState = $flight2->getFlightArrivalAirportState();
$arvAirportCode = $flight2->getFlightArrivalAirportCode();
$arvTime = $flight2->getFlightArrivalTime();

$flightNumber = $flight2->getFlightNumber();
$aircraftReg = $flight2->getFlightAircraftRegistration();
$model = $flight2->getFlightModelName();

$flightDuration = $arvTime - $deptTime;

echo "
        <span style='color:#4A90A4;'><b>$deptDate</b></span><br />
        <div class='complete-info-table'>
            <div class='complete-info-i'>
                $deptCity,&nbsp;$deptState&nbsp;($deptAirportCode)&nbsp;&nbsp;to&nbsp;&nbsp;".
                "$arvCity,&nbsp;$arvState&nbsp;($arvAirportCode)
            </div>
            <div class='complete-info-i'>            
                DEPART&nbsp;$deptTime&nbsp;&nbsp;ARRIVE&nbsp;$arvTime&nbsp;&nbsp;(&nbsp;$flightDuration h&nbsp;)
            </div>";
echo "
            <div class='complete-info-i'>            
                FLIGHT&nbsp;$flightNumber&nbsp;|&nbsp;$aircraftReg&nbsp;|&nbsp;$model
            </div>";
echo " 
        </div>     
                                    </p>";

echo "
                                </div>
                                
                            </div>
                            
                        </div>
                      </div>
                  </div>
                  <div class='clear'></div>
            </div>
        </div>
       <div class='sp-page-r'>";

echo 
"       </div>
      <div class='clear'></div>
    </div>
</div>
<!-- /main-cont -->
</div>";

printScriptsBoilerplate();

echo 
"</body>  
</html>";
}
?>
