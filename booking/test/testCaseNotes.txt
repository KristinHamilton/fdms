/* Kristin Hamilton
 * created 04-Apr-2016
 * last modified 11-Apr-2016
 * 
 * Desc: notes for test cases to implement for Booking module; other notes.
 */
 
 [Booking]
 
 - SEAT SELECTION
     - Notes: 
         - seat selection is optional for customer
         - there must be at least one traveler, but there may be more than one
         - there may be more than one flight to their trip (ex. flight from sacramento to 
           austin goes from sacramento to denver, then from denver to austin)
     - Test cases: 
         - seat selection is empty
         - seat selection has been chosen for each traveler for every flight in trip
         - seat selection has been chosen for each traveler for just one flight (of multiple) 
           in trip
         - seat selection has been chosen for some but not all travelers for one or more 
           flights in trip
     - In cases where customer has not made a seat selection for all travelers for every 
       flight in trip, will need to look up available seats on flight then assign seats
           - Possible considerations: 
               - If traveler is flying alone, look for single openings on plane to place 
                 them in
               - If traveler is flying with others, look for possible seats that are 
                 clustered or adjacent to each other
 
 - TRAVELER INFO
 - PAYMENT INFO
 - CONFIRM PAYMENT
 - BOOKING CONFIRMATION
 - PRINT CONFIRMATION PAGE
