<?php
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

class CustomerImpl implements Customer
{
    function __construct($customerInfo)
    {
        $this->customerUsername = $customerInfo['username'];
        $this->customerFirstName = $customerInfo['firstName'];
        $this->customerLastName = $customerInfo['lastName'];
        $this->customerEmail = $customerInfo['email'];
        $this->customerPhone = $this->removePhonePunctuation($customerInfo['phone']);
        $this->customerDoB = array(
            'day'=>$customerInfo['DoB']['day'],
            'month'=>$customerInfo['DoB']['month'],
            'year'=>$customerInfo['DoB']['year']
        );
    }

    function setCustomerEmail($email)
    {
        //if(strlen($this->customerEmail) == 0)
        //{
            $this->customerEmail = $email;
            $query = "UPDATE User SET email = '$this->customerEmail' 
                WHERE username = '$this->customerUsername';";
            $db = dbConnect();
            $result = mysqli_query($db, $query) or 
                die("<br />Error: unable to update customerEmail<br />");
            mysqli_close($db);
        //}
    }
    
    function setCustomerPhone($phone)
    {
       // if(strlen($this->customerPhone) == 0)
        //{
            $this->customerPhone = $this->removePhonePunctuation($phone);
            $query = "UPDATE User SET phone = '$this->customerPhone'
                WHERE username = '$this->customerUsername';";
            $db = dbConnect();
            $result = mysqli_query($db, $query) or
                die("<br />Error: unable to update customerPhone<br />");
            mysqli_close($db);
        //}
    }
    
    function getCustomerUsername()
    {
        return $this->customerUsername;
    }
    
    function getCustomerFirstName()
    {
        return $this->customerFirstName;
    }
    
    function getCustomerLastName()
    {
        return $this->customerLastName;
    }
    
    function getCustomerEmail()
    {
        return $this->customerEmail;
    }
    
    function getCustomerPhone($includePunctuation)
    {
        $phone = $this->customerPhone;
        if(!$includePunctuation) return $phone;
        
        $formattedPhone = "";
        if(strlen($phone) == 7 || strlen($phone) == 10 || strlen($phone) == 11)
        {
            $digitIndex = 0;
            
            // country code
            if(strlen($phone) == 11)
            {
                $formattedPhone .= "+". substr($phone, $digitIndex, ($digitIndex + 1)) ." ";
                $digitIndex++;
            }
            
            // area code
            if(strlen($phone) >= 10)
            {
                $formattedPhone .= "(". substr($phone, $digitIndex, ($digitIndex + 3)) .") ";
                $digitIndex += 3;
                //echo "<br />formattedPhone = $formattedPhone<br />";
            }

            // first three digits of 7-digit phone number 
            $formattedPhone .= substr($phone, $digitIndex, ($digitIndex)) ."-";
            $digitIndex += 3;
            //echo "<br />formattedPhone = $formattedPhone<br />";
            
            // last four digits
            $formattedPhone .= substr($phone, $digitIndex, ($digitIndex + 3));
            //echo "<br />formattedPhone = $formattedPhone<br />";
        }  
            
        return $formattedPhone;
    }
    
    function getCustomerDoB()
    {
        return $this->customerDoB;
    }
    
    function removePhonePunctuation($phone)
    {
        $validatedPhone = "";
        $digits_r = preg_split("[^0-9]", $phone);
        foreach($digits_r as $i) $validatedPhone .= $i;        
        return $validatedPhone;
    }
}
?>