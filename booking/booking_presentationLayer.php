<?php 
//session_start();
/* Kristin Hamilton
 * created 29-Mar-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

require_once "../util/utilFunctions.php";

function displayForm_seatSelection($booking)
{
    $self = htmlentities($_SERVER['PHP_SELF']);
    printPageHeader("Booking - Seat Selection", "Booking / Seat Selection");
    
    /*echo "<div>displayForm_seatSelection(): \$_POST = <br />";
    var_dump($_POST);
    echo "<br />displayForm_seatSelection(): \$_SESSION = <br />";
    var_dump($_SESSION);
    echo "</div>";*/
    
    $travelerCount = $booking->getFlight1()->getFlightTravelerCount();
    $seatSelection = array();

    echo
    "        <div class='sp-page'>
            <div class='sp-page-a'>
                <div class='sp-page-l'>
                      <div class='sp-page-lb'>
                          <div class='sp-page-p'>
                            <div class='booking-left'>";
    
    echo "
    <form method = 'post' action = '$self'>
        <button class='booking-complete-btn' name='submitSeatSelection'>Submit</button>
    </form>";
    
    echo
    "                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>
            </div>";
    echo
    "           <div class='sp-page-r'>";
    
    printCheckoutColumn($booking);
    
    echo
    "            </div>
               <div class='clear'></div>
        </div>
    </div>
    <!-- /main-cont -->
</div>";
    
    printScriptsBoilerplate();
    
    echo
    "</body>
    </html>";
}

// 
function displayForm_travelerInfo($booking)
{
    $self = htmlentities($_SERVER['PHP_SELF']);
    printPageHeader("Booking - Traveler Information", "Booking / Traveler Information");
    
    /*echo "<div>displayForm_travelerInfo(): \$_POST = <br />";
    var_dump($_POST);
    echo "<br />displayForm_travelerInfo(): \$_SESSION = <br />";
    var_dump($_SESSION);
    echo "</div>";*/
    
    $customer = $booking->getCustomer();
    $travelerCount = $booking->getFlight1()->getFlightTravelerCount();
    $emptyPassengerInfo = array(
        'firstName'=>'',
        'lastName'=>'',
        'DoB'=>array(
            'day'=>-1,
            'month'=>-1,
            'year'=>-1
        )        
    );

    echo
    "        <div class='sp-page'>
            <div class='sp-page-a'>
                <div class='sp-page-l'>
                      <div class='sp-page-lb'>
                          <div class='sp-page-p'>
                            <div class='booking-left'>";
    
    echo "<form method='post' action='$self'>
    <h2>Passenger Information</h2>";

   /* echo "
    <a href='#' onclick='addPassengerInfoForm();' class='add-passanger'>Add Passenger</a>
    <div class='clear'></div>";*/
    
    echo     "<span name='passengerInfo'><div class='booking-form'>";
    
    printPassengerInfoForm($customer);  // TODO: "add passenger"
    for($i = 0; $i < $travelerCount - 1; $i++)
    {
        printPassengerInfoForm($emptyPassengerInfo);
    }   
    
    echo "
    <div id='additionalPassengers'>&nbsp;</div>
</div>";
    
    if(isset($_SESSION['status']) && $_SESSION['status'] == 1)
    {
        echo "        
        <h2>Customer Information</h2>
            
        <div class='booking-form'>
            <div class='booking-form-i'>
                <label>First Name:</label>
                <div class='input'>
                    <input type='text' name='customerFirstName' value='".
                    $customer->getCustomerFirstName() ."'>
                </div>
            </div>
                            
            <div class='booking-form-i'>
                <label>Last Name:</label>
                <div class='input'>
                    <input type='text' name='customerLastName' value='". 
                    $customer->getCustomerLastName() ."'>
                </div>
            </div>
                            
            <div class='booking-form-i'>
                <label>Email Address:</label>
                <div class='input'>
                    <input type='text' name='customerEmailAddress' value='".
                    $customer->getCustomerEmail() ."'>
                </div>
            </div>
                            
            <div class='booking-form-i'>
                <label>Confirm Email Address:</label>
                <div class='input'><input type='text' name='customerEmailAddressConfirm' value='".
                $customer->getCustomerEmail() ."'></div>
            </div>
                        
            <div class='booking-form-i' style='float:left;'>
                <label>Preferred Phone Number:</label>
                <div class='input'><input type='text' name='customerPhoneNumber' value='".
                $customer->getCustomerPhone(true) ."'></div>
            </div>
                        
            <div class='booking-form-i'></div>
            <div class='clear'></div>
                        
        </div>                   
        </span>
                        
        <div class='booking-devider no-margin'></div>";
    }

    echo "
<button class='booking-complete-btn' name='submitTravelerInfo'>Submit</button>
</form>";
    
    echo
    "                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>
            </div>";
    echo
    "           <div class='sp-page-r'>";
    
    printCheckoutColumn($booking);
    
    echo
    "            </div>";
    echo
    "            <div class='clear'></div>
        </div>
    </div>
    <!-- /main-cont -->
</div>";

    printScriptsBoilerplate();
    
    echo 
    "</body>
    </html>";
}

function printPassengerInfoForm($customer = "")
{
    $firstName = "";
    $lastName = "";
    $dobDay = -1;
    $dobMonth = -1;
    $dobYear = -1;
    $currentYear = date('Y');
    
    if(is_object($customer))
    {
        $firstName = $customer->getCustomerFirstName();
        $lastName = $customer->getCustomerLastName();
        $dobDay = $customer->getCustomerDoB()['day'];
        $dobMonth = $customer->getCustomerDoB()['month'];
        $dobYear = $customer->getCustomerDoB()['year'];
    }
    
    echo "
    <div class='booking-form-i'>
        <label>First Name:</label>
        <div class='input'>
            <input type='text' name='passengerFirstName[]' value='$firstName' />
        </div>
    </div>
    <div class='booking-form-i'>
        <label>Last Name:</label>
        <div class='input'>
            <input type='text' name='passengerLastName[]' value='$lastName' />
        </div>
    </div>              
    <div class='booking-form-i'>
        <div class='form-calendar' style='float:left;'>
            <label>Date of Birth:</label>
            <div class='form-calendar-a'>
                <select class='custom-select' name='passengerDobDay[]'>";
                    printDropDownOptions_ints(1, 31, 'Day', $dobDay);
    echo "
                </select>
            </div>
            <div class='form-calendar-a'>
                <select class='custom-select' name='passengerDobMonth[]'>";
                    printDropDownOptions_ints(1, 12, 'Month', $dobMonth);
    echo "
                </select>
            </div>
            <div class='form-calendar-b'>
                <select class='custom-select' name='passengerDobYear[]'>";
                    printDropDownOptions_ints($currentYear - 100, $currentYear, 'Year', $dobYear);
    echo "
                </select>
            </div>
        </div>
        <div class='clear'></div>
    </div>
    <div class='booking-form-i'></div>
    <div class='clear'></div>
    <div class='booking-devider'></div>";
}

//
function displayForm_paymentInfo($paymentError, $booking)
{
    $self = htmlentities($_SERVER['PHP_SELF']);
    printPageHeader("Booking - Payment Information", "Booking / Payment Information");
        
    /*echo "<div>displayForm_paymentInfo(): \$_POST = <br />";
    var_dump($_POST);
    echo "<br />displayForm_paymentInfo(): \$_SESSION = <br />";
    var_dump($_SESSION);
    echo "</div>";*/
    
    $customer = $booking->getCustomer();  //getCustomerInfoFromPostArray(); 
    $paymentMethodTabToPrepopulate = '';
    $paymentInfo = array(
        'paymentTypeName' => '', 
        'cardNumber' => '',
        'cardholderName' => '',
        'cardExpiration' => array(
            'month' => -1,
            'year' => -1                    
        ) ,
        'cardSecurityCode'=>''
    );

    // if payment form was submitted, but payment failed to process successfully, display 
    // error, along with the fields populated as they had been submitted
    if($paymentError)
    {
        echo "<br /><span style='color:#FF0000;'>Error: Unable to process payment.</span><br />";
        $bookingPayment = $booking->getBookingPayment();
        $paymentInfo['paymentTypeName'] = $bookingPayment->getBookingPaymentTypeName();
        $paymentInfo['cardNumber'] = $bookingPayment->getBookingPaymentCardNumber();
        $paymentInfo['cardholderName'] = $bookingPayment->getBookingPaymentCardholderName();
        $paymentInfo['cardExpiration']['month'] = $bookingPayment->getBookingPaymentCardExpirationMonth();
        $paymentInfo['cardExpiration']['year'] = $bookingPayment->getBookingPaymentCardExpirationYear();
        $paymentInfo['cardSecurityCode'] = $bookingPayment->getBookingPaymentCardSecurityCode();
    }
    else
    {
        // if displaying payment page for the first time and user is logged in, prepopulate 
        // payment fields for preferred payment method or for last used payment method, if 
        // possible
        if(isset($_SESSION['status']) && $_SESSION['status'] == '1')
        {
            //if(isset($_SESSION['username']))
            //{
            $username = $customer->getCustomerUsername();
            $paymentInfo = getDefaultSavedPaymentMethodInfo($username);
                //echo "<br />paymentInfo = "; var_dump($paymentInfo); echo "<br />";
            //}
        }
    }
    
    echo
    "        <div class='sp-page'>
            <div class='sp-page-a'>
                <div class='sp-page-l'>
                      <div class='sp-page-lb'>
                          <div class='sp-page-p'>
                            <div class='booking-left'>";
    echo                         
"                                <div class='booking-devider no-margin'></div>";
    
    
    echo "
                                <h2>How would you like to pay?</h2>
    
                                <div class='payment-wrapper'>
                                    <div class='payment-tabs'>\n";
    
    // pre-select appropriate payment type tab
    $cardHtmlTemplate = "<a href='#'%s onclick=\"setPaymentTypeName('%s');\">%s</a>\n";
    if((strnatcasecmp($paymentInfo['paymentTypeName'], "")) != 0 || 
       (strnatcasecmp($paymentInfo['paymentTypeName'], "Debit") == 0))
    {
        $paymentMethodTabToPrepopulate = 'Debit';
        echo sprintf($cardHtmlTemplate, "", "Credit", "Credit Card ");
        echo sprintf($cardHtmlTemplate, " class='active'", "Debit", "Debit Card ");
    }
    else 
    {
        $paymentMethodTabToPrepopulate = 'Credit';
        echo sprintf($cardHtmlTemplate, " class='active'", "Credit", "Credit Card ");
        echo sprintf($cardHtmlTemplate, "", "Debit", "Debit Card ");
    }   
        
    echo "
                                    </div>
                                    <div class='clear'></div>
                                    <div class='payment-tabs-content'>";
    
    echo "                            <form method='post' action='$self'>";

    printPaymentTab("Credit", $paymentMethodTabToPrepopulate, $paymentInfo);
    printPaymentTab("Debit", $paymentMethodTabToPrepopulate, $paymentInfo);
    
    echo "                                     
                                <div class='booking-complete'>";  
            echo "
    <button class='booking-complete-btn' id='submitPaymentInfo' name='submitPaymentInfo' value='credit'>COMPLETE BOOKING</button>";
    echo "                                </div>
            </div>
                                </div>
            </form>";
    
    echo
    "   
                            </div>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>
            </div>";
    echo
    "           <div class='sp-page-r'>";

    printCheckoutColumn($booking);
    
    echo
    "            </div>
                <div class='clear'></div>
        </div>
    </div>
    <!-- /main-cont -->
</div>";
            
    printScriptsBoilerplate();
    
    echo 
    "</body>  
    </html>";
}

function printPaymentTab($paymentMethod, $paymentMethodTabToPrepopulate, $savedPaymentInfo)
{
    //echo "<br />savedPaymentInfo = "; var_dump($savedPaymentInfo); echo "<br />";
    $paymentMethodLower = strtolower($paymentMethod);
    $currentYear = date('Y');
    $paymentInfo = array(
        'paymentTypeName' => '', 
        'cardNumber' => '',
        'cardholderName' => '',
        'cardExpiration' => array(
            'month' => -1,
            'year' => -1                    
        ) ,
        'cardSecurityCode'=>''
    );
    
    if(strnatcasecmp($paymentMethod, $paymentMethodTabToPrepopulate) == 0)
    {
        $paymentInfo = $savedPaymentInfo;
        //echo "<br />payment method == paymentMethodTabToPrepopulate: ". $paymentMethod ." == ". $paymentMethodTabToPrepopulate;
        //echo "<br /> prepopulating fields for ". $paymentMethod ." payment method <br />";
    }
    
    echo "
                                        <!-- // -->
                                        <div class='payment-tab'>
                                            <div class='payment-type'>";
    echo "
                                            </div>
                                            <div class='booking-form'>
                                                <div class='booking-form-i'>
                                                    <label>Card Number:</label>
                                                    <div class='input'><input type='text' name='cardNumber_$paymentMethodLower' value='".$paymentInfo['cardNumber']."'></div>
                                                </div>
                                                <div class='booking-form-i'>
                                                    <label>Cardholder Name:</label>
                                                    <div class='input'><input type='text' name='cardholderName_$paymentMethodLower' value='".$paymentInfo['cardholderName']."'></div>
                                                </div>
                                                <div class='booking-form-i'>
                                                    <label>Expiration Date:</label>
                                                    <div class='card-expiration'>
                                                        <select class='custom-select' name='cardExpirationMonth_$paymentMethodLower'>";
    
    printDropDownOptions_ints(1, 12, 'Month', $paymentInfo['cardExpiration']['month']);
    
    echo
    "                                                        </select>
                                                    </div>
                                                    <div class='card-expiration'>
                                                        <select class='custom-select card-year' name='cardExpirationYear_$paymentMethodLower'>";
    
    printDropDownOptions_ints($currentYear, $currentYear + 20, 'Year', $paymentInfo['cardExpiration']['year']);
    
    echo
    "                                                        </select>
                                                    </div>
                                                    <div class='clear'></div>
                                                </div>
                                                <div class='booking-form-i'>
                                                    <label>Card Security Code:</label>
                                                    <div class='inpt-comment'>
                                                        <div class='inpt-comment-l'>
                                                            <div class='inpt-comment-lb'>
                                                                <div class='input'><input type='text' name='cardSecurityCode_$paymentMethodLower' value='". $paymentInfo['cardSecurityCode'] ."'></div>
                                                            </div>
                                                            <div class='clear'></div>
                                                        </div>
                                                    </div>";
    echo "
                                                    <div class='clear'></div>
                                                </div>
                                            </div>
                                            <div class='clear'></div>";
    echo "
                                            <div class='checkbox'>
                                                <label>
                                                      <input type='checkbox' name='termsAndConditions_$paymentMethodLower' value='' />
                                                      I accept the <a href='#'>Terms &amp; Conditions</a>
                                                </label>
                                            </div>
    
                                        </div>
                                        <!-- \\ -->";
}

function displayForm_confirmPayment($booking)
{
    $self = htmlentities($_SERVER['PHP_SELF']);
    printPageHeader("Booking - Confirm Payment", "Booking / Confirm Payment");
    
    /*echo "<div>displayForm_confirmPayment(): \$_POST = <br />";
    var_dump($_POST);
    echo "<br />displayForm_confirmPayment(): \$_SESSION = <br />";
    var_dump($_SESSION);
    echo "</div>";*/
    
    $flight1 = $booking->getFlight1();
    $flight2 = $booking->getFlight2();
    $bookingPayment = $booking->getBookingPayment();

    echo
    "        <div class='sp-page'>
            <div class='sp-page-a'>
                <div class='sp-page-l'>
                      <div class='sp-page-lb'>
                          <div class='sp-page-p'>
                            <div class='booking-left'>";
    
    echo "<h2>Payment Total</h2>
            Subtotal&nbsp;&nbsp;&nbsp;\$". round($booking->getBasePriceTotal()) ."<br /><br />".
            "Taxes &amp; Fees&nbsp;&nbsp;&nbsp;\$". round($booking->getTaxesAndFeesTotal(), 2) ."<br /><br />".
            "<b>Total&nbsp;&nbsp;&nbsp;\$". round($booking->getFinalPriceTotal(), 2) ."</b><br />".
            "<br /><br /><br />".
           "<h2>Payment Method</h2>
            Payment Method&nbsp;&nbsp;&nbsp;". $bookingPayment->getBookingPaymentTypeName() ."<br /><br />".
            "Card Number &nbsp;&nbsp;&nbsp;**** **** **** ".
                substr($bookingPayment->getBookingPaymentCardNumber(), 12, 15) ."<br /><br />".
            "Cardholder Name &nbsp;&nbsp;&nbsp;". $bookingPayment->getBookingPaymentCardholderName() ."<br /><br />".
            "Card Expiration &nbsp;&nbsp;&nbsp;". 
                $bookingPayment->getBookingPaymentCardExpirationMonth() ."/". 
                $bookingPayment->getBookingPaymentCardExpirationYear() ."<br /><br />".
            "Security Code &nbsp;&nbsp;&nbsp;";
                $secCode = $bookingPayment->getBookingPaymentCardSecurityCode();
                if(strlen($secCode) == 4) echo "****";
                else echo "***";
                echo "<br /><br />";
    echo "
    <form method = 'post' action = '$self'>
        <button class='booking-complete-btn' name='submitConfirmPayment'>Confirm and Submit Payment</button>
    </form>";
    
    echo
    "    
                            </div>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>
            </div>";
    echo
    "           <div class='sp-page-r'>";

    printCheckoutColumn($booking);
    
    echo
    "        </div>
             <div class='clear'></div>
        </div>
    </div>
    <!-- /main-cont -->
</div>";
            
    printScriptsBoilerplate();
    
    echo 
    "</body>  
    </html>";
}

function printSubmitButton($name, $value)
{   
    echo "<input type = 'submit' name = '$name' value = '$value'>";
}
?>