<?php
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

class FlightImpl implements Flight
{
    function __construct($flightID, $travelerCount)
    {
        $flightInfo = getFlightInfo($flightID);
        
        $this->flightID = $flightID;
        $this->travelerCount = $travelerCount; 
        
        $this->flightNumber = $flightInfo['flightNumber'];
        $this->basePrice = $flightInfo['price'];
        $this->aircraftRegistration = $flightInfo['aircraftRegistration'];
        $this->model = $flightInfo['modelName'];
        
        $this->arrivalDate = $flightInfo['arrivalInfo']['date'];
        $this->arrivalTime = $flightInfo['arrivalInfo']['time'];
        $this->arrivalAirportID = $flightInfo['arrivalInfo']['airport']['id'];
        $this->arrivalAirportCode = $flightInfo['arrivalInfo']['airport']['code'];
        $this->arrivalAirportName = $flightInfo['arrivalInfo']['airport']['name'];
        $this->arrivalAirportAddress = $flightInfo['arrivalInfo']['airport']['address'];
        $this->arrivalAirportCity = $flightInfo['arrivalInfo']['airport']['city'];
        $this->arrivalAirportState = $flightInfo['arrivalInfo']['airport']['state'];
        $this->arrivalAirportZip = $flightInfo['arrivalInfo']['airport']['zip'];
        
        $this->departureDate = $flightInfo['departureInfo']['date'];
        $this->departureTime = $flightInfo['departureInfo']['time'];
        $this->departureAirportID = $flightInfo['departureInfo']['airport']['id'];
        $this->departureAirportCode = $flightInfo['departureInfo']['airport']['code'];
        $this->departureAirportName = $flightInfo['departureInfo']['airport']['name'];
        $this->departureAirportAddress = $flightInfo['departureInfo']['airport']['address'];
        $this->departureAirportCity = $flightInfo['departureInfo']['airport']['city'];
        $this->departureAirportState = $flightInfo['departureInfo']['airport']['state'];
        $this->departureAirportZip = $flightInfo['departureInfo']['airport']['zip'];
    }

    function getFlightID()
    {
        return $this->flightID;
    }
    
    function getFlightTravelerCount()
    {
        return $this->travelerCount;
    }
    
    function getFlightNumber()
    {
        return $this->flightNumber;
    }
    
    function getFlightPrice()
    {
        return $this->basePrice;
    }
    
    function getFlightAircraftRegistration()
    {
        return $this->aircraftRegistration;
    }
    
    function getFlightModelName()
    {
        return $this->model;
    }
    
    /* accessors for flight arrival info */
    
    function getFlightDepartureDate($formatDate)
    {
        if($formatDate) return $this->getFormattedDate($this->departureDate);
        else return $this->departureDate;
    }
    
    function getFlightDepartureTime()
    {
        return $this->departureTime;
    }
    
    function getFlightDepartureAirportID()
    {
        return $this->departureAirportID;
    }
    
    function getFlightDepartureAirportCode()
    {
        return $this->departureAirportCode;
    }
    
    function getFlightDepartureAirportName()
    {
        return $this->departureAirportName;
    }
    
    function getFlightDepartureAirportAddress()
    {
        return $this->departureAirportAddress;
    }
    
    function getFlightDepartureAirportCity()
    {
        return $this->departureAirportCity;
    }
    
    function getFlightDepartureAirportState()
    {
        return $this->departureAirportState;
    }
    
    function getFlightDepartureAirportZip()
    {
        return $this->flightDepartureAirportZip;
    }
    
    /* accessors for flight arrival info */
    
    function getFlightArrivalDate($formatDate)
    {
        if($formatDate) return $this->getFormattedDate($this->arrivalDate);
        else return $this->arrivalDate;
    }
    
    function getFlightArrivalTime()
    {
        return $this->arrivalTime;
    }
    
    function getFlightArrivalAirportID()
    {
        return $this->arrivalAirportID;
    }
    
    function getFlightArrivalAirportCode()
    {
        return $this->arrivalAirportCode;
    }
    
    function getFlightArrivalAirportName()
    {
        return $this->arrivalAirportName;
    }
    
    function getFlightArrivalAirportAddress()
    {
        return $this->arrivalAirportAddress;
    }
    
    function getFlightArrivalAirportCity()
    {
        return $this->arrivalAirportCity;
    }
    
    function getFlightArrivalAirportState()
    {
        return $this->arrivalAirportState;
    }
    
    function getFlightArrivalAirportZip()
    {
        return $this->flightArrivalAirportZip;
    }
    
    // $unformattedDate ex: 'yyyy-mm-dd'
    // ex: converts '2016-04-27' to 'Wednesday | April 27, 2016'
    function getFormattedDate($unformattedDate)
    {
        $date_r = explode("-", $unformattedDate);
        $month = $date_r[2];
        $day = $date_r[1];
        $year = $date_r[0];
        $date = date_create($month."-".$day."-".$year);
        
        $dayOfWeek = date_format($date, "l");
        $monthName = date_format($date, "F");
        
        return "$dayOfWeek | $monthName $day, $year";
    }
}
?>