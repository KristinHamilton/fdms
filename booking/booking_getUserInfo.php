<?php
//session_start(); 
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

require_once "../util/utilFunctions.php";
require_once "db.php";
require_once "../flightbooking/flightDatabaseFunctions.php";

// date of birth in format: "yyyy-mm-dd". convert to array.
function splitUserDobIntoDMY($dob)
{
    if(is_array($dob))
    {
        return $dob;
    } 

    $customerDob = array('day'=>-1, 'month'=>-1, 'year'=>-1);
    $splitDob = explode("-", $dob);
    
    $customerDob['year'] = $splitDob[0];
    $customerDob['month'] = $splitDob[1];
    $customerDob['day'] = $splitDob[2];
    
    return $customerDob;
}

function getUserIdNumber($username)
{
    $userID = "";
    $query = "SELECT userID FROM User WHERE username = '$username';";
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain userID<br />");
    if($result) $userID = mysqli_fetch_assoc($result)['userID'];
    mysqli_close($db);
    return $userID;
}

function getCustomerInfo($username)
{
    $customerInfo = array(
            'firstName' => '',
            'lastName' => '',
            'email' => '',
            'phone' => '',
            'DoB' => array(
                'day' => -1,
                'month' => -1,
                'year' => -1));
    
    $query = "SELECT firstName, lastName, email, phone, 
    DATE_FORMAT(DoB, '%d') AS 'day', 
    DATE_FORMAT(DoB, '%m') AS 'month', 
    DATE_FORMAT(DoB, '%Y') AS 'year' 
    FROM User WHERE username = '$username';";
    
    $db = dbConnect();
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain customerInfo<br />");
    
    while($row = mysqli_fetch_assoc($result))
    {
        $customerInfo['firstName'] = $row['firstName'];
        $customerInfo['lastName'] = $row['lastName'];
        $customerInfo['email'] = $row['email'];
        $customerInfo['phone'] = $row['phone'];

        $customerInfo['DoB']['day'] = $row['day'];
        $customerInfo['DoB']['month'] = $row['month'];
        $customerInfo['DoB']['year'] = $row['year'];
    }
    
    mysqli_close($db);
    return $customerInfo;
}

function getDefaultSavedPaymentMethodInfo($username)
{
    $db = dbConnect();
    
    $userID = -1;
    $paymentID = -1;
    $paymentInfo = array(
        'paymentTypeName' => '',
        'cardNumber' => '',
        'cardholderName' => '',
        'cardExpiration' => array(
            'month' => -1,
            'year' => -1
        ) ,
        'cardSecurityCode'=>''
    );
    
    // get userID for payment query
    $userID = getUserIdNumber($username);
    
    // get ID of most recent payment made by user
    $query = "SELECT paymentID FROM Payment 
    WHERE userID = $userID 
    ORDER BY paymentDate DESC LIMIT 1;";
    $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain userID<br />");
    
    if($result)
    {
        $paymentID = mysqli_fetch_assoc($result)['paymentID'];
    }

    // if a payment was found for user, get info for that payment to be used to prepopulate
    // payment information form
    if($paymentID > -1)
    {        
        $query = "SELECT * FROM PaymentMethod WHERE userID = $userID LIMIT 1;";
        $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain paymentInfo<br />");
        
        while($row = mysqli_fetch_assoc($result))
        {
            $paymentTypeID = $row['paymentTypeID'];
            $paymentInfo['cardNumber'] = $row['cardNumber'];
            $paymentInfo['cardholderName'] = $row['cardHolderName'];
            $paymentInfo['cardExpiration']['month'] = $row['expirationMonth'];
            $paymentInfo['cardExpiration']['year'] = $row['expirationYear'];
            $paymentInfo['cardSecurityCode'] = $row['securityCode'];
            
            $query = "SELECT paymentTypeName FROM PaymentType 
            WHERE paymentTypeID = $paymentTypeID;";
            echo $query;
            $result = mysqli_query($db, $query) or die("<br />Error: unable to obtain paymentTypeName<br />");
            if($result)
            {
                $paymentTypeName = mysqli_fetch_assoc($result)['paymentTypeName'];
                $paymentInfo['paymentTypeName'] = strtoupper($paymentTypeName[0]).
                                                  substr($paymentTypeName, 1);
            }
        }
    }

    mysqli_close($db);
    return $paymentInfo;
}