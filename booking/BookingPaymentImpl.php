<?php
/* Kristin Hamilton */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

class BookingPaymentImpl implements BookingPayment
{
    function __construct($bookingPaymentInfo)
    {
        $this->bookingPaymentTypeName = $bookingPaymentInfo['paymentTypeName'];
        $this->bookingPaymentCardNumber = $bookingPaymentInfo['cardNumber'];
        $this->bookingPaymentCardholderName = $bookingPaymentInfo['cardholderName'];
        $this->bookingPaymentCardExpirationMonth = $bookingPaymentInfo['cardExpiration']['month'];
        $this->bookingPaymentCardExpirationYear = $bookingPaymentInfo['cardExpiration']['year'];
        $this->bookingPaymentCardSecurityCode = $bookingPaymentInfo['cardSecurityCode'];
    }
    
    function getBookingPaymentTypeName()
    {
        return $this->bookingPaymentTypeName;
    }
    
    function getBookingPaymentCardNumber()
    {
        return $this->bookingPaymentCardNumber;
    }
    
    function getBookingPaymentCardholderName()
    {
        return $this->bookingPaymentCardholderName;
    }
    
    function getBookingPaymentCardExpirationMonth()
    {
        return $this->bookingPaymentCardExpirationMonth;
    }
    
    function getBookingPaymentCardExpirationYear()
    {
        return $this->bookingPaymentCardExpirationYear;
    }
    
    function getBookingPaymentCardSecurityCode()
    {
        return $this->bookingPaymentCardSecurityCode;
    }
}
?>