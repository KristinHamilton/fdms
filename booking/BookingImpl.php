<?php
/* Kristin Hamilton
 * created 04-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

class BookingImpl implements Booking 
{
    // constructs new object instance
    function __construct($flight1, $flight2)
    {
        $this->flight1 = $flight1;
        $this->flight2 = $flight2;
        
        $this->seatSelection = array();
        $this->customer = array();
        $this->passengerList = array();
        
        $this->bookingPayment = array();
        $this->basePriceTotal = 0.0;
        $this->taxesAndFeesTotal = 0.0;
        $this->finalPriceTotal = 0.0;    

        $pricing = new PricingImpl($this);//TODO
        $this->setBasePriceTotal($pricing->getBasePrice());
        $this->setTaxesAndFeesTotal($pricing->getTaxesAndFees());//TODO
        $this->setFinalPriceTotal();
        
        $this->confirmationNumber = "";
    }

    /* mutator methods */
    
    // sets seat selection for this booking
    // returns nothing
    function setSeatSelection($seatSelection)
    {
        if(!isEmpty($seatSelection))
        {
            $this->seatSelection = $seatSelection;
        }
        else
        {
            //TODO...will need to choose for the customer
        }
        return;
    }

    function setCustomer($customer)
    {
        $this->customer = $customer;
    }
    
    // sets passenger info for this booking
    // returns nothing
    function setPassengerList($passengerList)
    {
        $this->passengerList = $passengerList;
    }

    // sets payment info for this booking
    // returns nothing
    function setBookingPayment($bookingPayment)
    {
        $this->bookingPayment = $bookingPayment;
    }

    function setBasePriceTotal($basePriceTotal)
    {
        $this->basePriceTotal = $basePriceTotal;
    }
    
    function setTaxesAndFeesTotal($taxesAndFeesTotal)
    {
        $this->taxesAndFeesTotal = $taxesAndFeesTotal;
    }
    
    function setFinalPriceTotal()
    {
        $this->finalPriceTotal = $this->basePriceTotal + $this->taxesAndFeesTotal;
    }
    
    function setConfirmationNumber($confNum)
    {
        if($this->confirmationNumber == "") 
            $this->confirmationNumber = $this->getNewConfirmationNumber();
    }
    
    /* accessor methods */

    function getCustomer()
    {
        return $this->customer;
    }
    
    // returns seat selection for this booking
    function getSeatSelection()
    {
        return $this->seatSelection;
    }

    // returns passenger list for this booking
    function getPassengerList()
    {
        return $this->passengerList;
    }

    // returns payment info for this booking
    function getBookingPayment()
    {
        return $this->bookingPayment;
    }
    
    function getBasePriceTotal()
    {
        return $this->basePriceTotal;
    }
    
    function getFinalPriceTotal()
    {
        return $this->finalPriceTotal;
    }
    
    function getTaxesAndFeesTotal()
    {
        return $this->taxesAndFeesTotal;
    }
    
    function getFlight1()
    {
        return $this->flight1;
    }
    
    function getFlight2()
    {
        return $this->flight2;
    }

    function getConfirmationNumber()
    {
        return $this->confirmationNumber;
    }
    
    // returns true if payment processed successfully; otherwise, returns false
    function processPayment($paymentInfo)
    {
        $paymentSuccessful = $this->confirmationNumber == "";  // TODO
        if($paymentSuccessful == true) 
            $this->setConfirmationNumber($this->getNewConfirmationNumber());
        return $paymentSuccessful;
    }
    
    // returns a unique 16-character confirmation number: first 16 chars of md5 hash of 
    // current UTC datetime
    function getNewConfirmationNumber()
    {
        return substr(md5(time()), 1, 16);
    }
}
?>