<?php
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

interface Flight
{
    function getFlightID();
    function getFlightTravelerCount();
    function getFlightNumber();
    function getFlightPrice();
    function getFlightAircraftRegistration();
    function getFlightModelName();
    
    function getFlightDepartureDate($formatDate);
    function getFlightDepartureTime();
    function getFlightDepartureAirportID();
    function getFlightDepartureAirportCode();
    function getFlightDepartureAirportName();
    function getFlightDepartureAirportAddress();
    function getFlightDepartureAirportCity();
    function getFlightDepartureAirportState();
    function getFlightDepartureAirportZip();
    
    function getFlightArrivalDate($formatDate);
    function getFlightArrivalTime();
    function getFlightArrivalAirportID();
    function getFlightArrivalAirportCode();
    function getFlightArrivalAirportName();
    function getFlightArrivalAirportAddress();
    function getFlightArrivalAirportCity();
    function getFlightArrivalAirportState();
    function getFlightArrivalAirportZip();
}
?>