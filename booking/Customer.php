<?php
/* Kristin Hamilton */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

interface Customer
{
    function setCustomerEmail($email);
    function setCustomerPhone($phone);
    
    function getCustomerUsername();
    function getCustomerFirstName();
    function getCustomerLastName();
    function getCustomerEmail();
    function getCustomerPhone($includePunctuation);
    function getCustomerDoB();
}
?>