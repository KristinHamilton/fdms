<?php
/* Kristin Hamilton
 * created 25-Apr-2016
 */
//echo phpinfo();
ini_set ("display_errors", "1");
error_reporting(E_ALL);

spl_autoload_register(function($className){
    include $className . ".php";
});

interface BookingPayment
{
    function getBookingPaymentTypeName();
    function getBookingPaymentCardNumber();
    function getBookingPaymentCardholderName();
    function getBookingPaymentCardExpirationMonth();
    function getBookingPaymentCardExpirationYear();    
    function getBookingPaymentCardSecurityCode();
}
?>